package moteur;

import java.util.Observable;
import java.util.Observer;
import v1.command.ICommand;
import v1.command.Tic;
import v1.materiel.IHorloge;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public class Moteur extends Observable implements IMoteur{

    private int tempo;
    private int compteur =0;
    private int mesure;
    private boolean enMarche;

    private IHorloge horloge;

    private ICommand cmdTempo;
    private ICommand cmdMesure;
    private ICommand cmdTic;

    /**
     * Constructeur du moteur
     * @param tempo
     * @param mesure
     * @param horloge
     */
    public Moteur(int tempo, int mesure, IHorloge horloge){
        setCmdTiC(new Tic(this));
        this.enMarche =false;
        this.horloge=horloge;
        this.tempo=tempo;
        this.mesure=mesure;
    }

    @Override
    public int getTempo() {
        return this.tempo;
    }

    @Override
    public void setTempo(int tempo) {
        if(this.enMarche){
            setEnMarche(false);
            this.compteur =0;
            this.tempo=tempo;
            this.setChanged();
            this.notifyObservers();
            setEnMarche(true);
        }
    }

    @Override
    public boolean getEnMarche() {
        return this.enMarche;
    }

    @Override
    public void setEnMarche(boolean enMarche) {
        this.enMarche = enMarche;
        if(enMarche){
            long delay=60000/tempo;
            horloge.activerPeriodiquement(cmdTic, delay);
        }
        else{
            horloge.desactiver(cmdTic);
        }
    }

    @Override
    public int getMesure() {
        return this.mesure;
    }

    @Override
    public void setMesure(int mesure) {
        if(mesure>1 && mesure<10){
            this.mesure=mesure;
            this.setChanged();
            this.notifyObservers();
        }
    }

    @Override
    public ICommand getCmdTempo() {
        return this.cmdTempo;
    }

    @Override
    public void setCmdTempo(ICommand cmd) {
        this.cmdTempo = cmd;
    }

    @Override
    public ICommand getCmdMesure() {
        return this.cmdMesure;
    }

    @Override
    public void setCmdMesure(ICommand cmd) {
        this.cmdMesure = cmd;
    }

    @Override
    public ICommand getCmdTic() {
        return this.cmdTic;
    }

    @Override
    public void setCmdTiC(ICommand cmd) {
        this.cmdTic = cmd;
    }

    @Override
    public void tic() {
        if(compteur > mesure-2){
            compteur =0;
            cmdMesure.execute();
        }
        else{
            compteur++;
            cmdTempo.execute();
        }
    }

    @Override
    public void notifyObservers() {
        super.notifyObservers();
    }

     @Override
    public synchronized void addObserver(Observer o) {
        super.addObserver(o);
    }
}
