package v1.command;

/**
 *
 * Interface de base pour les patterns commands
 *
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public interface ICommand {
    
    void execute();
    
}
