package v1.command;

import controller.IController;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public class Mesure implements ICommand{

    private IController ctrl;

    /**
     * Constructeur
     *
     * @param controller - Le contrôleur sur lequel il faudra exécuter la commande.
     */
    public Mesure(IController controller) {
        this.ctrl = controller;
    }

    @Override
    public void execute() {
        this.ctrl.battreMesure();
    }
}