package v1.command;

import controller.IController;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public class Tempo implements ICommand{

    private IController ctrl;

    public Tempo(IController ctrl) {
        this.ctrl = ctrl;
    }

    @Override
    public void execute() {
        this.ctrl.battreTempo();
    }
}