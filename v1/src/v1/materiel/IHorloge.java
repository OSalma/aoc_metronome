package v1.materiel;

import v1.command.ICommand;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public interface IHorloge {

    /**
    * Appel périodique d'une cmd et 
    * l'ajoute a la liste des commandes en cours d'executin.
    * 
    * @param cmd la commande a exécuter périodiquement.
    * @param periodeEnSecondes la période d'éxécution de la commande.
    * @since 1
    */
    void activerPeriodiquement(ICommand cmd, long periodeEnSecondes);
	
    /**
    * Désactive la commande activé précédemment par activerPeriodiquement.
    * 
    * @param cmd la commande à désactiver.
    * @since 1
    */
    void desactiver(ICommand cmd);
}
