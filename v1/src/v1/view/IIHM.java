package v1.view;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public interface IIHM {
    
    public void majTempo();
    
    public void setTempo(int tempo);
    
    public void setMesure(int mesure);
    
    public void incMesure();
    
    public void decMesure();
    
    public void start();
    
    public void stop();
    
    public void clignoter_Tempo();
    
    public void clignoter_Mesure();
    
    public void jouer_son_tempo();
    
    public void jouer_son_mesure();
}
