package v1.view;

import controller.IController;
import controller.Controller;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import son.IGestionnaire_son;
import v1.materiel.Horloge;
import moteur.Moteur;
import son.MakeSound;

import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public class Ctrl_Java_Fx implements IIHM {
    @FXML
    TextField tempo_field;
    @FXML
    Slider tempo_slider;
    @FXML
    TextField mesure_field;
    @FXML
    Circle tempo_led;
    @FXML
    Circle mesure_led;

    private IGestionnaire_son sound;
    private IController controller;

    public Ctrl_Java_Fx() {
        sound = new MakeSound();
        controller = new Controller(new Moteur(50, 4, new Horloge()), this);
    }

    public void majTempo() {
        controller.setTempoMoteur((int) tempo_slider.getValue());
    }


    public void setTempo(int tempo) {
        tempo_slider.setValue(tempo);
        tempo_field.setText(Integer.toString(tempo));
    }


    public void setMesure(int mesure) {
        mesure_field.setText(Integer.toString(mesure));
    }


    public void clignoter_Tempo() {
        tempo_led.setFill(Color.RED);
        Timer time=new Timer();
        TimerTask task=new TimerTask() {
            public void run() {
                tempo_led.setFill(Color.DODGERBLUE);
            }
        };
        time.schedule(task, 225);
    }


    public void clignoter_Mesure() {
        mesure_led.setFill(Color.GREEN);
        Timer time=new Timer();
        TimerTask task=new TimerTask() {
            public void run() {
                mesure_led.setFill(Color.DODGERBLUE);
            }
        };
        time.schedule(task, 225);
    }


    public void jouer_son_tempo(){
        Timer time=new Timer();
        TimerTask task=new TimerTask() {
            public void run() {
                sound.playSound("src/son/tick.wav");
            }
        };
        time.schedule(task, 225);

    }


    public void jouer_son_mesure(){
        Timer time=new Timer();
        TimerTask task=new TimerTask() {
            public void run() {
                sound.playSound("src/son/tock.wav");
            }
        };
        time.schedule(task, 225);

    }


    @FXML
    public void incMesure() {
        controller.incrementer();
    }

    @FXML
    public void decMesure() {
        controller.decrementer();
    }

    @FXML
    public void start() {
        controller.start();
    }

    @FXML
    public void stop() {
        controller.stop();
    }

}
