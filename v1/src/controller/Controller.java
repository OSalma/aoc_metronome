package controller;

import java.util.Observable;
import java.util.Observer;
import v1.command.Mesure;
import v1.command.Tempo;
import moteur.Moteur;
import v1.view.IIHM;


/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public class Controller implements IController,Observer{

    private Moteur moteur;
    private IIHM ihm;

    public Controller(Moteur moteur, IIHM ihm) {
        this.moteur = moteur;
        this.ihm = ihm;
        this.moteur.addObserver(this);

        moteur.setCmdMesure(new Mesure(this));
        moteur.setCmdTempo(new Tempo(this));
    }

    @Override
    public int getTempoMoteur() {
        return this.moteur.getTempo();
    }

    @Override
    public void setTempoMoteur(int tempo) {
        this.moteur.setTempo(tempo);
    }

    @Override
    public void start() {
        if(!this.moteur.getEnMarche()){
            this.moteur.setEnMarche(true);
        }
    }

    @Override
    public void stop() {
        if(this.moteur.getEnMarche()){
            this.moteur.setEnMarche(false);
        }
    }

    @Override
    public void incrementer() {
        this.moteur.setMesure(this.moteur.getMesure() + 1);
    }

    @Override
    public void decrementer() {
        this.moteur.setMesure(this.moteur.getMesure() - 1);
    }

    @Override
    public void traitment() {
        this.moteur.tic();
    }

    @Override
    public void battreMesure() {
        this.ihm.jouer_son_mesure();
        this.ihm.clignoter_Mesure();
    }

    @Override
    public void battreTempo() {
        this.ihm.jouer_son_tempo();
        this.ihm.clignoter_Tempo();
    }

    @Override
    public void update(Observable o, Object arg) {
        ihm.setTempo(moteur.getTempo());
        ihm.setMesure(moteur.getMesure());
    }
}