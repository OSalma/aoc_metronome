package controller;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public interface IController {

    /**
    * Retourne le tempo affecté au moteur
    *
    * @return le tempo
    * @since 1
    */
    int getTempoMoteur();

    /**
    * Méthode appelée pour affecter la valeur du tempo au moteur.
    *
     * @param tempo
    * @since 1
    */
    void setTempoMoteur(int tempo);

    /**
    * Méthode appelée pour start.
    *
    * @since 1
    */
    void start();

    /**
    * Méthode appelée pour stop.
    *
    * @since 1
    */
    void stop();

    /**
    * Méthode appelée pour incrementer la mesure.
    *
    * @since 1
    */
    void incrementer();

    /**
    * Méthode appelée pour decrementer la mesure.
    *
    * @since 1
    */
    void decrementer();

    /**
    * Méthode appelée pour faire marcher le moteur
    *
    * @since 1
    */
    void traitment();

    /**
    * Méthode appelée pour jouer le son et
    * clignoter la mesure
    *
    * @since 1
    */
    void battreMesure();

    /**
    * Méthode appelée pour jouer le son et
    * clignoter le tempo
    *
    * @since 1
    */
    void battreTempo();

}
