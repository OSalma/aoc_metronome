package moteur;

import v2.command.ICommand;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public interface IMoteur {

    /**
     * Retourne le tempo
     *
     * @return le tempo
     * @since 1
     */
    int getTempo();

    /**
     * Définit la valeur du tempo
     *
     * @param tempo la nouvelle valeur du tempo
     * @since 1
     */
    void setTempo(int tempo);

    /**
     * Retourne l'état du moteur
     *
     * @return True si le moteur est en marche, False sinon.
     * @since 1
     */
    boolean getEnMarche();

    /**
     * Définit l'état du moteur.
     *
     * @param enMarche True pour faire marcher le moteur, False pour l'arret.
     * @since 1
     */
    void setEnMarche(boolean enMarche);

    /**
     * Retourne le nombre mesure.
     *
     * @return le nombre mesure.
     * @since 1
     */
    int getMesure();

    /**
     * Définit le nombre de mesure.
     *
     * @param mesure la nouvelle mesure.
     * @since 1
     */
    void setMesure(int mesure);

    /**
     * Retourne la commande tempo
     *
     * @return la commande tempo
     * @since 1
     */
    ICommand getCmdTempo();

    /**
     * Définit la command tempo
     *
     * @param cmd la command pour le tempo
     * @since 1
     */
    void setCmdTempo(ICommand cmd);

    /**
     * Retourne la command mesure
     *
     * @return la command mesure
     * @since 1
     */
    ICommand getCmdMesure();

    /**
     * Définit la command mesure
     *
     * @param cmd la commande pour la mesure
     * @since 1
     */
    void setCmdMesure(ICommand cmd);

    /**
     * Retourne la command tic
     *
     * @return la command tic
     * @since 1
     */
    ICommand getCmdTic();

    /**
     * Définit la command tic
     *
     * @param cmd la commande pour tic
     * @since 1
     */
    void setCmdTiC(ICommand cmd);

    /**
     * Méthode exécutée périodiquement via la commande "Tic". Elle
     * exécute les commandes "MarquerTemps" et "MarqueurMesure".
     *
     * @since 1
     */
    void tic();
}
