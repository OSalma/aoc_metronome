package v2.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public class MainV2 extends Application {

   @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("metronome.fxml"));
        primaryStage.setTitle("AOC Metronome V2 de Salma Ouhammouch");
        primaryStage.setScene(new Scene(root, 600, 300));
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
