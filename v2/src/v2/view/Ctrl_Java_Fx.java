package v2.view;

import controller.IController;
import controller.Controller;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import v2.materiel.Materiel;
import v2.materiel.Molette;
import v2.view.IIHM;
import moteur.Moteur;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import v2.command.Pull;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public class Ctrl_Java_Fx extends Observable implements IIHM, IIHM_pull {
    @FXML
    TextField tempo_field;
    @FXML
    Slider tempo_slider;
    @FXML
    TextField mesure_field;
    @FXML
    Circle tempo_led;
    @FXML
    Circle mesure_led;

    private IController ctrl;
    private float old_molette = (float) 0.5;

    public Ctrl_Java_Fx() {
    	ctrl = new Controller(new Moteur(50, 4, Materiel.getHorloge()), this);
        Materiel.getHorloge().activerPeriodiquement(new Pull(this), 500);
        this.addObserver((Molette) Materiel.getMolette());
    }

    public void majTempo() {
    	setChanged();
        this.notifyObservers((float)tempo_slider.getValue()/100);
    }

    public void setTempo(int tempo) {
    	 Materiel.getAfficheur().afficherTempo(tempo);
         tempo_field.setText(Integer.toString(tempo));
    }

    public void setMesure(int mesure) {
        mesure_field.setText(Integer.toString(mesure));
    }

    public void clignoter_Tempo() {
    	Materiel.getAfficheur().allumerLED(0);
        tempo_led.setFill(Color.RED);
        Timer time = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
            	Materiel.getAfficheur().eteindreLED(0);
                tempo_led.setFill(Color.DODGERBLUE);
            }
        };
        time.schedule(task, 240);
    }

    public void clignoter_Mesure() {
    	Materiel.getAfficheur().allumerLED(1);
        mesure_led.setFill(Color.GREEN);
        Timer time = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
                Materiel.getAfficheur().eteindreLED(1);
                mesure_led.setFill(Color.DODGERBLUE);
            }
        };
        time.schedule(task, 240);
    }


    public void jouer_son_tempo(){
        Timer time=new Timer();
        TimerTask task=new TimerTask() {
            public void run() {
                Materiel.getEmeteur_tempo().emettreClic();
            }
        };
        time.schedule(task, 240);
    }


    public void jouer_son_mesure(){
    	Timer time=new Timer();
        TimerTask task=new TimerTask() {
            public void run() {
                Materiel.getEmeteur_mesure().emettreClic();
            }
        };
        time.schedule(task, 240);
    }

    @FXML
    public void start() {
    	Materiel.getClavier().setButton(1, true);
    }

    @FXML
    public void stop() {
    	Materiel.getClavier().setButton(2, true);
    }

    @FXML
    public void incMesure() {
       Materiel.getClavier().setButton(3, true);
    }

    @FXML
    public void decMesure() {
    	Materiel.getClavier().setButton(4, true);
    }

	@Override
	public void push_change() {
        if(Materiel.getClavier().touchePressee(1)){
            ctrl.start();
        }
        if(Materiel.getClavier().touchePressee(2)){
            ctrl.stop();
        }
        if(Materiel.getClavier().touchePressee(3)){
            ctrl.incrementer();
        }
        if(Materiel.getClavier().touchePressee(4)){
            ctrl.decrementer();
        }
        if(Materiel.getMolette().position() != old_molette) {
            old_molette = Materiel.getMolette().position();
            ctrl.setTempoMoteur((int) (Materiel.getMolette().position() * 100));
        }
	}

}
