package v2.view;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public interface IIHM {

	/**
	 * since 1
	 */
    public void majTempo();

    /**
     *
     * @param tempo
	 * since 1
	 */
    public void setTempo(int tempo);

    /**
     *
     * @param mesure
	 * since 1
	 */
    public void setMesure(int mesure);

    /**
	 * since 1
	 */
    public void start();

    /**
	 * since 1
	 */
    public void stop();

    /**
	 * since 1
	 */
    public void incMesure();

    /**
	 * since 1
	 */
    public void decMesure();

    /**
	 * since 1
	 */
    public void clignoter_Tempo();

    /**
	 * since 1
	 */
    public void clignoter_Mesure();

    /**
	 * since 1
	 */
    public void jouer_son_tempo();

    /**
	 * since 1
	 */
    public void jouer_son_mesure();
}
