package v2.view;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public interface IIHM_pull {
	/**
	 * Contrôle sur l'état du clavier.
	 *
	 * since 2
	 */
    void push_change();
}
