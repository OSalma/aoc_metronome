package v2.materiel;

import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import v2.command.ICommand;


/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public class Horloge implements IHorloge{

    private final Hashtable<ICommand, Timer> table_command_timer =new Hashtable<>();

    @Override
    public void activerPeriodiquement(ICommand cmd, long periodeEnSecondes) {
        if(table_command_timer.get(cmd)==null){
            Timer timer=new Timer();
            TimerTask task=new TimerTask() {
                public void run() {
                    cmd.execute();
                }
            };
            timer.schedule(task, 0, periodeEnSecondes);
            table_command_timer.put(cmd, timer);
        }
    }

    @Override
    public void desactiver(ICommand cmd) {
        if(table_command_timer.get(cmd)!=null){
            table_command_timer.get(cmd).cancel();
            table_command_timer.remove(cmd);
        }
    }
}
