package v2.materiel;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public interface IClavier {

	/**
    * Retourne true si le le bouton i est enfoncé,
    * false s'il est relaché.
    * 1 = start, 2 = stop, 3 = inc, 4 = dec.
    *
    * @param i
    * @return etat
    * @since 2
    */
    boolean touchePressee(int i);

    /**
     * Affecte au bouton un état qui est boolean.
     *
     *@param button
     * @param state.
     * @since 2
     */
    void setButton(int button, boolean state);
}
