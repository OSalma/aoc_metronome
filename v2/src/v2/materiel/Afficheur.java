package v2.materiel;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public class Afficheur implements IAfficheur {

	@Override
    public void allumerLED(int numLED) {
        System.out.println("Materiel : Afficheur : LED "+numLED+" allumée");
    }

    @Override
    public void eteindreLED(int numLED) {
        System.out.println("Materiel : Afficheur : LED "+numLED+" éteinte");
    }

    @Override
    public void afficherTempo(int tempo) {
        System.out.println("Materiel : Afficheur : Tempo : "+tempo);
    }
}
