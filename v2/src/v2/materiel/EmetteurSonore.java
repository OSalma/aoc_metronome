package v2.materiel;

import son.IGestionnaire_son;
import son.MakeSound;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public class EmetteurSonore implements IEmetteurSonore {

    private static IGestionnaire_son sound_player = new MakeSound();
    private String sound_path;

    public EmetteurSonore(String sound_path) {
        this.sound_path = sound_path;
    }

    @Override
    public void emettreClic() {
        sound_player.playSound(sound_path);
    }
}