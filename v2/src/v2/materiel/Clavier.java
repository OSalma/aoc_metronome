package v2.materiel;

import java.util.ArrayList;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public class Clavier implements IClavier{

    private ArrayList<Boolean> touches = new ArrayList<Boolean>();

    public Clavier() {
        touches.add(false);
        touches.add(false);
        touches.add(false);
        touches.add(false);
        touches.add(false);
    }

    public void setButton(int button, boolean state) {
        touches.set(button, state);
    }

    @Override
    public boolean touchePressee(int i) {
        if(touches.get(i)){
            touches.set(i, false);
            return true;
        }
        return false;
    }
}
