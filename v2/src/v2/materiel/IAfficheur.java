package v2.materiel;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public interface IAfficheur {

	/**
    * Affiche a la console le numéro de LED allumé.
    *
    * @param numLED.
    * @since 2
    */
    void allumerLED(int numLED);

    /**
     * Affiche a la console le numéro de LED eteind.
     *
     * @param numLED.
     * @since 2
     */
    void eteindreLED(int numLED);

    /**
     * Affiche a la console la valeur du tempo.
     *
     * @param tempo.
     * @since 2
     */
    void afficherTempo(int tempo);
}
