package v2.materiel;

import v2.materiel.Horloge;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public class Materiel {

    private static IAfficheur afficheur = new Afficheur();
    private static IEmetteurSonore emeteur_tempo = new EmetteurSonore("src/son/tick.wav");
    private static IEmetteurSonore emeteur_mesure = new EmetteurSonore("src/son/tock.wav");
    private static IMolette molette = new Molette((float)0.5);
    private static IClavier clavier = new Clavier();
    private static IHorloge horloge = new Horloge();

    public static IHorloge getHorloge(){
        return horloge;
    }

    public static IClavier getClavier(){
        return clavier;
    }

    public static IAfficheur getAfficheur() {
        return afficheur;
    }

    public static IEmetteurSonore getEmeteur_tempo() {
        return emeteur_tempo;
    }

    public static IEmetteurSonore getEmeteur_mesure() {
        return emeteur_mesure;
    }

    public static IMolette getMolette() {
        return molette;
    }
}
