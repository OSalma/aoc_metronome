package v2.materiel;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public interface IMolette {

	/**
    * Retourne la position de la molette,
    * entre 0.0 et 1.0,
    * en virgule flotante.
    *
    *@return position
    * @since 2
    */
    float position();
}
