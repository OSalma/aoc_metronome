package v2.materiel;

import java.util.Observable;
import java.util.Observer;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public class Molette implements IMolette, Observer {

    private float position;

    public Molette(float position) {
        this.position = position;
    }

    public void setPosition(float position){
        System.out.println("Materiel : Molette : Position " + position);
        System.out.println("Materiel : Molette : Tempo : " + position*100);
        this.position = position;
    }

    @Override
    public void update(Observable o, Object arg) {
        setPosition((float) arg);
    }

    @Override
    public float position() {
        return this.position;
    }
}