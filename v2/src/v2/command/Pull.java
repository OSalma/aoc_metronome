package v2.command;

import v2.command.ICommand;
import v2.view.IIHM_pull;

/**
*
* @author Ouhammouch Salma M2 MITIC
*/
public class Pull implements ICommand {

    /**
     * Variable de stockage privée qui stocke un pointeur sur l'IHM_Hardware.
     */
    private IIHM_pull iihm_pull;

    /**
     * Constructeur
     *
     * @param iihm_pull
     */
    public Pull(IIHM_pull iihm_pull) {
        this.iihm_pull = iihm_pull;
    }

    @Override
    public void execute() {
    	iihm_pull.push_change();
    }
}
