package v2.command;

import moteur.Moteur;

/**
 *
 * @author Ouhammouch Salma M2 MITIC
 */
public class Tic implements ICommand{

    private Moteur moteur;

    public Tic(Moteur moteur) {
        this.moteur = moteur;
    }

    @Override
    public void execute() {
        this.moteur.tic();
    }

}
